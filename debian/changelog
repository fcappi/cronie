cronie (1.5.5-3) experimental; urgency=medium

  * Add Hurd-workaround-for-PATH_MAX.patch (Closes: #638048)
  * Fix spelling error in previous changelog entry

 -- Christian Kastner <ckk@debian.org>  Tue, 05 Nov 2019 08:04:37 +0100

cronie (1.5.5-2) experimental; urgency=medium

  * Don't build /usr/sbin/anacron
    /usr/sbin/anacron is still provided by src:anacron.
    Thanks, Andreas Beckmann, for catching this! (Closes: #944024)

 -- Christian Kastner <ckk@debian.org>  Sun, 03 Nov 2019 11:12:33 +0100

cronie (1.5.5-1) experimental; urgency=medium

  * New upstream version 1.5.5
  * Drop patches (included upstream):
    - crond-report-missing-newline-before-EOF.patch
    - crontab-Add-Y-N-to-retry-prompt.patch
    - crontab-fsync-to-check-for-full-disk.patch
    - crontab.1-Various-fixes-and-improvements.patch
    - entries-Explicitly-validate-upper-ranges-and-steps.patch
  * Refresh patches
    - debian/patches/Manpage-and-typo-fixes.patch
    - debian/patches/Rename-PAM-service-to-cronie.patch
      + Split out Unbundle-upstream-PAM-config.patch from this one

 -- Christian Kastner <ckk@debian.org>  Thu, 31 Oct 2019 22:20:05 +0100

cronie (1.5.4-final-2) experimental; urgency=medium

  * build: Set default EDITOR to /usr/bin/sensible-editor
  * d/patches (added):
    - crond-report-missing-newline-before-EOF.patch
    - entries-Explicitly-validate-upper-ranges-and-steps.patch
    - crontab.1-Various-fixes-and-improvements.patch
    - crontab-Add-Y-N-to-retry-prompt.patch
    - crontab-fsync-to-check-for-full-disk.patch

 -- Christian Kastner <ckk@debian.org>  Wed, 30 Oct 2019 21:12:01 +0100

cronie (1.5.4-final-1) experimental; urgency=medium

  * New upstream release. (Closes: #697811, #783856)

  [ Andreas Henriksson ]
  * debian/watch: update for cronie move to github
  * Modify patches to apply against new upstream release
  * Add debian/gbp.conf
  * Adjust and ship the cronie.service file
  * Use debian/clean to remove src/cron-paths.h
  * Fix lintian warning about not using default-mta
  * Fix typo in patch tagging meta-header

  [ Christian Kastner ]
  * d/control:
    - Switch Build-Depends from debhelper to debhelper-compat
    - Bump debhelper compatibility level to 12
    - Bump Standards-Version to 4.4.1 (no changes needed)
    - Remove now obsolete d/compat file
    - Set Rules-Requires-Root: no
      We don't need (fake)root for building the package.
    - Point Homepage to GitHub
    - Set Vcs-* URLs for Salsa
    - Mark package cronie as Multi-Arch: foreign
    - Add Pre-Depends: ${misc:Pre-Depends} to binary package
      As recommended by lintian's skip-systemd-native-flag-missing-pre-depends
  * d/cronie.default: Add new daemon flag "-P"
  * d/rules:
    - Add hardening flags
    - Stop passing actions to dh_installinit
      This has been obsoleted by dependency-based booting in Wheezy, see
      https://lists.debian.org/debian-devel/2013/05/msg01109.html
    - Use DEB_HOST_ARCH_OS from /usr/share/dpkg/architecture.mk
    - Proper passing of CFLAGS through DEB_CFLAGS_MAINT_APPEND
  * d/copyright:
    - Fix syntax errors
    - Switch URL to official policy URL
    - Point Source to GitHub
      fedorahosted.org has been retired
    - Bump copyrights
  * d/clean: Remove generated files for (non-enabled) anacron build
  * Sync maintscripts with src:cron

 -- Christian Kastner <ckk@debian.org>  Mon, 28 Oct 2019 19:35:38 +0100

cronie (1.4.8-1~exp1) experimental; urgency=low

  * Initial release (Closes: #590876)
  * debian/patches added:
    - 0001-Unbundle-anacron
    - 0002-Manpage-and-typo-fixes
    - 0003-Rename-PAM-service-to-cronie
    - 0004-Debian-specific-paths-and-features
    - 0005-Extend-support-for-kFreeBSD-and-GNU-HURD

 -- Christian Kastner <debian@kvr.at>  Tue, 26 Jul 2011 14:00:34 +0200
